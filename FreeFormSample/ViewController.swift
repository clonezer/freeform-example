//
//  ViewController.swift
//  FreeFormSample
//
//  Created by Peerasak Unsakon on 11/11/19.
//  Copyright © 2019 Peerasak Unsakon. All rights reserved.
//

import UIKit

class ViewController: FreeFormViewController {

    var section1 = FreeFormSection(tag: "Demo1", title: "Section 1")
    var section2 = FreeFormSection(tag: "Demo2", title: "Section 2")
    var section3 = FreeFormSection(tag: "Demo3", title: "Section 3")
    
    let pushOptions = FreeFormPushRow(tag: "Push", title: "Seletect Somethings", value: "Thai", options: ["Thai", "English"])
    let text = FreeFormTextRow(tag: "Text", title: "Fullname", value: "Peerasak Unsakon" as AnyObject)
    let datetime = FreeFormDatetimeRow(tag: "Datetime", title: "Date", selectedDate: Date(),
                                       min: Calendar.current.date(byAdding: .day, value: -10, to: Date()),
                                       max: Calendar.current.date(byAdding: .day, value: 10, to: Date()))
    let segmented = FreeFormSegmentedRow(tag: "Segmented", title: "Segmented", value: "Hello", options: ["Hello", "Bye!"])
    let button = FreeFormButtonRow(tag: "Button", title: "Tap Me!!")
    let stepper = FreeFormStepperRow(tag: "Stepper", title: "Stepper", max: 10, min: 0, value: 0)
    let switchRow = FreeFormSwitchRow(tag: "Switch", title: "Show or Hide", value: true as AnyObject?)
    
    override func initializeForm() {
        
        self.form.title = "FreeForm Example"
        
        self.section1 = {
            let section = self.section1
            section.addRow(self.pushOptions)
            section.addRow(self.datetime)
            section.addRow(self.button)
            
            section.customHeader = { titleView in
                titleView.backgroundColor = .systemGray
            }
            
            section.headerHeight =  40
            
            return section
        }()
        
        self.section2 = {
            let section = self.section2
            section.addRow(segmented)
            section.customHeader = { titleView in
                titleView.backgroundColor = .systemGray2
            }
            
            section.headerHeight =  40
            return section
        }()
        
        self.section3 = {
            let section = self.section3
            section.addRow(self.switchRow)
            section.addRow(self.stepper)
            section.addRow(self.text)
            
            section.customHeader = { titleView in
                titleView.backgroundColor = .systemGray3
            }
            
            section.headerHeight =  40
            return section
        }()
        
        self.form.addSection(self.section1)
        self.form.addSection(self.section2)
        self.form.addSection(self.section3)
        
        self.segmented.customCell = { cell in
            guard let segmentedCell = cell as? FreeFormSegmentedCell else { return }
            segmentedCell.segmentedControl.tintColor = UIColor.red
        }
        
        self.segmented.didChanged = { value, row in
            guard let text = value as? String else { return }
            if text == "Bye!" {
                self.stepper.hidden = true
                self.text.height = 150
            }else {
                self.stepper.hidden = false
                self.text.height = 80
            }
            self.form.reloadForm(true)
        }
        
        self.button.customCell = { cell in
            guard let buttonCell = cell as? FreeFormButtonCell else { return }
            buttonCell.backgroundColor = UIColor.cyan
        }
        
        self.button.didTapped = { row in
            guard let buttonCell = row.cell as? FreeFormButtonCell else { return }
            if buttonCell.button.titleLabel?.text == "Tap Me!!" {
                buttonCell.button.setTitle("I'm a Button", for: .normal)
            }else {
                buttonCell.button.setTitle("Tap Me!!", for: .normal)
            }
        }
        
        self.switchRow.didChanged = { value, row in
            guard let isOn = value as? Bool else {
                return
            }
            self.button.hidden = !isOn
            self.form.reloadForm(true)
        }
    }


}

